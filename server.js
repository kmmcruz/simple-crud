const express = require('express');
const path = require('path');
const app = express(),
      port = 3080;

app.use(express.urlencoded({extended: true}));
app.use(express.json()) 
// To parse the incoming requests with JSON payloads

// place holder for the data
let tasks = [
  {
    id: 1,
    task: 'task1',
    assignee: 'assignee1000',
    status: 'completed'
  },
  {
    id: 2,
    task: 'task2',
    assignee: 'assignee1001',
    status: 'completed'
  },
  {
    id: 3,
    task: 'task3',
    assignee: 'assignee1002',
    status: 'completed'
  },
  {
    id: 4,
    task: 'task4',
    assignee: 'assignee1000',
    status: 'completed'
  }
];


const getTaskDetails = (taskId) => {
    return tasks.filter(t => t.id === parseInt(taskId))
}

app.get('/tasks', (req, res) => {
  console.log('api/tasks called!')
  res.json(tasks);
});

app.get('/task/:taskId', (req, res) => {
    console.log(`task id is: ${req.params}`)
    const task = getTaskDetails(req.params.taskId)
    res.json(task);
});

app.post('/addTask/', (req, res) => {
    console.log(`task id is: ${req.body.taskId}`)
    let data = {
        id: req.body.taskId,
        task: req.body.task,
        assignee: req.body.assignee,
        status: req.body.status
    }
    tasks.push(data)
    res.json(data);
});

app.delete('/deleteTask/:taskId', (req, res) => {
    console.log(`task id is: ${req.params.taskId}`)
    // Get the index of the object in the array
    const indexOfObject = tasks.findIndex((task) => {
    // if the current object name key matches the string
    // return boolean value true
    if (task.id === parseInt(req.params.taskId)) {
      return true;
    }
    // else return boolean value false
    return false;
    });

    tasks.splice(indexOfObject,indexOfObject)
    res.json(tasks);
});

app.put('/updateTask/:taskId', (req, res) => {
    console.log(`task id is: ${req.params.taskId}`)
    let data = {
        id: parseInt(req.params.taskId),
        task: req.body.task,
        assignee: req.body.assignee,
        status: req.body.status
    }
    const indexOfObject = tasks.findIndex((task) => {
        // if the current object name key matches the string
        // return boolean value true
        if (task.id === parseInt(req.params.taskId)) {
          return true;
        }
        // else return boolean value false
        return false;
    });
    tasks[indexOfObject] = {...data};
    res.json(tasks);
});

app.get('/', (req,res) => {
  res.send(`<h1>API Running on the port ${port}</h1>`);
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});